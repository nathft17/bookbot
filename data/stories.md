## Generated Story 1448383084692465280
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Today"}
    - slot{"day": "Today"}
    - utter_ask_hour_start
* choose_hour
    - slot{"hour_start":"12 pm"}
    - utter_ask_duration
* choose_duration{"duration": "1 hour"}
    - slot{"duration": "1 hour"}
    - utter_ask_room
* choose_room{"name_room": "red"}
    - slot{"name_room": "red"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart

## Generated Story 2292873877784887244
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Today"}
    - slot{"day": "Today"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "3 am"}
    - slot{"hour_start": "3 am"}
    - utter_ask_duration
* choose_duration{"duration": "1 hour"}
    - slot{"duration": "1 hour"}
    - utter_ask_room
* choose_room{"name_room": "red"}
    - slot{"name_room": "red"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart


## Generated Story -286259384564690913
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Today"}
    - slot{"day": "Today"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "20"}
    - slot{"hour_start": "20"}
    - utter_ask_duration
* choose_duration{"duration": "20 minutes"}
    - slot{"duration": "20 minutes"}
    - utter_ask_room
* choose_room{"name_room": "red"}
    - slot{"name_room": "red"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart

## Generated Story 3058178446454365362
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Tommorow"}
    - slot{"day": "Tommorow"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "3:30 am"}
    - slot{"hour_start": "3:30 am"}
    - utter_ask_duration
* choose_duration{"duration": "1 hour"}
    - slot{"duration": "1 hour"}
    - utter_ask_room
* choose_room{"name_room": "blue"}
    - slot{"name_room": "blue"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart

## Generated Story 4599061510071362309
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Today"}
    - slot{"day": "Today"}
    - utter_ask_duration
* choose_hour{"hour_start": "12am"}
    - slot{"hour_start": "12am"}
    - utter_ask_duration
* choose_duration{"duration": "30 minutes"}
    - slot{"duration": "30 minutes"}
    - utter_ask_room
* choose_room{"name_room": "red"}
    - slot{"name_room": "red"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart

## Generated Story 8222233808718041810
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Tomorrow"}
    - slot{"day": "Tomorrow"}
    - utter_ask_duration
* choose_duration{"duration": "20 minutes"}
    - slot{"duration": "20 minutes"}
    - utter_ask_hour_start
* choose_hour
    - rewind
* choose_hour{"hour_start": "9:30 am"}
    - slot{"hour_start": "9:30 am"}
    - utter_ask_room
* choose_room{"name_room": "red"}
    - slot{"name_room": "red"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart

## Generated Story 1
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Tomorrow"}
    - slot{"day": "Tomorrow"}
    - utter_ask_duration
* choose_duration{"duration": "20 minutes"}
    - slot{"duration": "20 minutes"}
    - utter_ask_hour_start
* choose_hour
    - rewind
* choose_hour{"hour_start": "9:30 am"}
    - slot{"hour_start": "9:30 am"}
    - utter_ask_room
* choose_room{"name_room": "red"}
    - slot{"name_room": "red"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart


## Generated Story 2
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Tomorrow"}
    - slot{"day": "Tomorrow"}
    - utter_ask_duration
* choose_duration{"duration": "20 minutes"}
    - slot{"duration": "20 minutes"}
    - utter_ask_hour_start
* choose_hour
    - rewind
* choose_hour{"hour_start": "9:30 am"}
    - slot{"hour_start": "9:30 am"}
    - utter_ask_room
* choose_room{"name_room": "red"}
    - slot{"name_room": "red"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart


## Generated Story 3
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Tomorrow"}
    - slot{"day": "Tomorrow"}
    - utter_ask_duration
* choose_duration{"duration": "20 minutes"}
    - slot{"duration": "20 minutes"}
    - utter_ask_hour_start
* choose_hour
    - rewind
* choose_hour{"hour_start": "9:30 am"}
    - slot{"hour_start": "9:30 am"}
    - utter_ask_room
* choose_room{"name_room": "red"}
    - slot{"name_room": "red"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart

## Generated Story 2100047222126511072
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_room
    - utter_ask_day
* choose_day{"day": "Today"}
    - slot{"day": "Today"}
    - utter_ask_duration
* choose_duration{"duration": "20 minutes"}
    - slot{"duration": "20 minutes"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "2pm"}
    - slot{"hour_start": "2pm"}
    - utter_ask_room
* choose_room
    - rewind
* choose_room{"name_room": "Red"}
    - slot{"name_room": "Red"}
    - utter_check_infos
* information_inputed_correct
    - action_room
    - action_repeat
    - utter_thanks
    - action_restart

## Story book_vacation 1
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_vacation
    - utter_ask_day
* choose_day{"day": "Today"}
    - slot{"day": "Today"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "3 am"}
    - slot{"hour_start": "3 am"}
    - utter_ask_duration
* choose_duration{"duration": "1 hour"}
    - slot{"duration": "1 hour"}
    - utter_ask_room
    - utter_check_infos
* information_inputed_correct
    - action_vacation
    - action_repeat
    - utter_thanks
    - action_restart

## Story book_vacation 2
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_vacation
    - utter_ask_day
* choose_day{"day": "Tomorrow"}
    - slot{"day": "Tomorrow"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "8 am"}
    - slot{"hour_start": "8 am"}
    - utter_ask_duration
* choose_duration{"duration": "2 days"}
    - slot{"duration": "2 days"}
    - utter_ask_room
    - utter_check_infos
* information_inputed_correct
    - action_vacation
    - action_repeat
    - utter_thanks
    - action_restart

## Story book_vacation 3 (incorrect info)
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* book_vacation
    - utter_ask_day
* choose_day{"day": "Tomorrow"}
    - slot{"day": "Tomorrow"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "8 am"}
    - slot{"hour_start": "8 am"}
    - utter_ask_duration
* choose_duration{"duration": "2 days"}
    - slot{"duration": "2 days"}
    - utter_ask_room
    - utter_check_infos
* information_inputed_uncorrect
    - utter_ask_day
* choose_day{"day": "Tomorrow"}
    - slot{"day": "Tomorrow"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "8 am"}
    - slot{"hour_start": "8 am"}
    - utter_ask_duration
* choose_duration{"duration": "2 days"}
    - slot{"duration": "2 days"}
    - utter_ask_room
    - utter_check_infos
* information_inputed_correct
    - action_vacation
    - action_repeat
    - utter_thanks
    - action_restart


## Generated Story 769660202141297312
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* what_can_you_do
    - utter_usage
    - action_repeat
    - action_restart


## Generated Story 76966020214197312
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* what_can_you_do
    - utter_usage
    - action_repeat
    - action_restart


## Generated Story 769660202141227312
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* what_can_you_do
    - utter_usage
    - action_repeat
    - action_restart

## Generated Story 1915311378126915784
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* what_can_you_do
    - utter_usage
    - action_repeat
    - action_restart

## Generated Story -8624876866928606609
* greet
    - slot{"name":"nathalie"}
    - utter_greet
* what_can_you_do
    - utter_usage
    - action_repeat
    - action_restart


## Generated Story -6758967078627404096
* greet{"name": "nathalie"}
    - slot{"name": "nathalie"}
    - utter_greet
* book_vacation
    - utter_ask_day
* choose_day{"day": "Tomorrow"}
    - slot{"day": "Tomorrow"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "8am"}
    - slot{"hour_start": "8am"}
    - utter_ask_duration
* choose_duration{"duration": "2 days"}
    - slot{"duration": "2 days"}
    - utter_check_infos_vacations
* information_inputed_correct
    - action_vacation
    - action_repeat
    - utter_thanks
    - action_restart


## Generated Story 301
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_yearly
- utter_ask_yearly_insights_details
* insight_year
- action_get_insight
- action_restart


## Generated Story 302
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_yearly
- utter_ask_yearly_insights_details
* insight_year
- action_get_insight
- action_restart


## Generated Story 303
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_yearly
- utter_ask_yearly_insights_details
* insight_year
- action_get_insight
- action_restart


## Generated Story 304
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_yearly
- utter_ask_yearly_insights_details
* insight_year
- action_get_insight
- action_restart


## Generated Story 305
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_monthly
- utter_ask_monthly_insights_details
* insight_month
- action_get_insight
- action_restart


## Generated Story 306
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_monthly
- utter_ask_monthly_insights_details
* insight_month
- action_get_insight
- action_restart


## Generated Story 307
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_monthly
- utter_ask_monthly_insights_details
* insight_month
- action_get_insight
- action_restart


## Generated Story 308
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_monthly
- utter_ask_monthly_insights_details
* insight_month
- action_get_insight
- action_restart



## Generated Story 309
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_daily
- utter_ask_daily_insights_details
* insight_day
- action_get_insight
- action_restart


## Generated Story 310
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_daily
- utter_ask_daily_insights_details
* insight_day
- action_get_insight
- action_restart


## Generated Story 311
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_daily
- utter_ask_daily_insights_details
* insight_day
- action_get_insight
- action_restart


## Generated Story 312
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_daily
- utter_ask_daily_insights_details
* insight_day
- action_get_insight
- action_restart


## Generated Story 313
* greet
- utter_greet
* get_insight
- utter_ask_insight_details
* more_insight_daily
- utter_ask_daily_insights_details
* insight_day
- action_get_insight
- action_restart

## Generated Story 6506805079645222451
* greet{"name": "nathalie"}
    - slot{"name": "nathalie"}
    - utter_greet
* book_vacation
    - utter_ask_day
* choose_day{"day": "tomorrow"}
    - slot{"day": "tomorrow"}
    - utter_ask_hour_start
* choose_hour{"hour_start": "9am"}
    - slot{"hour_start": "9am"}
    - utter_ask_duration
* choose_duration{"duration": "2 days"}
    - slot{"duration": "2 days"}
    - utter_check_infos_vacations
* information_inputed_correct
    - action_vacation
    - action_repeat
    - utter_thanks
    - action_restart
