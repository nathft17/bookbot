## intent:book_room
- I'd like to reserve a room
- I would like to arrange a reservation room
- Can you reserve me a room ?
- Hey, I want a room !
- I want to book a room
- I would like to make a reservation for a room.
- I would like to reserve a room ?
- wishing to make a room reservation 
- book me a room
- reserve me a room


## intent: book_vacation
- I want to take vacation days
- I would like to request paid leave
- Can you help me to do a vacation request ? 
- Hey, I want vacation !
- I want to do a leave request
- help me to do a vacation request
- Can you help me to request paid leave ? 
- I want to have vacation days
- help me to register vacation days
- tell me how to register vacation days

## intent: ask_remaining_vacation_days
- How many remaining vacation days I have ? 
- I want to know how many remaining vacation days I have
- give me my remaining vacation days
- tell me how many remaining vacation days
- I want my remaining days
- Give me my remaining days
- I don't remember my remaining vacation days
- I want to know my remaining days
- can you tell me how many remaining days i still have ? 
- Remaining vacation days ? 
- tell me my remaining days

## intent:choose_day
- [today](day)
- [Tomorrow](day)
- [Today](day)
- [Now](day)
- [Next Monday](day)
- [Tuesday](day)
- [19/01/2019](day)
- [Today](day)
- [December, 3rd](day)
- on [July, 4th](day)
- [Tomorrow](day)
- [Today](day)

## intent:choose_duration
- I want my reunion to last for [30 minutes](duration)
- For [2 hours](duration)
- For [1 hour 30 minutes](duration)
- For [two hours](duration)
- For [1 hour](duration)
- For [2 hours](duration)
- For [20 minutes](duration)
- For [10 min](duration)
- For [20 minutes](duration)
- For [45 min](duration)

## intent:choose_hour
- I want to start my meeting at [16:30](hour_start)
- At [12:00](hour_start)
- At [9am](hour_start)
- [4pm](hour_start)
- At [12](hour_start)
- At [12pm](hour_start)
- At [4 am](hour_start)
- At [3 am](hour_start)
- At [20](hour_start)
- At [10am](hour_start)
- At [12](hour_start) am
- At [9:30 am](hour_start)
- At [2pm](hour_start)

## intent:choose_room
- I want to book the [blue](name_room)
- The [Red](name_room) room
- [Blue](name_room)
- [Green](name_room) room
- I wish to book the [red](name_room) room
- I would like to book the [yellow](name_room) room
- The [black](name_room) room
- The [red](name_room) room
- The [white](name_room) room
- The [blue](name_room) room
- The [green](name_room) room
- [Red](name_room)
- Can you book me the [green](name_room) room?
- The [red](name_room) room

## intent:greet
- Hi Workbot, I'm [Nathalie](name)
- Hello bot, My name is [nathalie](name)
- Hey bot, I'm [nisha](name)
- Hi, I'm [nathalie](name)
- My name is [nathalie](name)
- Hello bot, it's [Charles](name)
- Heyyyy Workbot, My name is [richard](name)
- Good morning bot, My name is [michel](name)
- Good evening bot, My name is [michel](name)
- Hellow bot, My name is [michel](name)


## intent:information_inputed_correct
- yes
- correct
- that's right
- yes it's good
- yes it's ok
- yes good job !
- yes it's that
- yes my dear
- yup ! it's that 
- good job ! 
- it's ok


## intent:information_inputed_uncorrect
- no
- unfortunately nope
- No it's a wrong information
- No you have a wrong information
- No it's bad :/
- Eh noooo
- It's not that 
- nope ! that wrong
- nope 
- unfortunately no

## intent:get_insight
- Can you give me an insight on the reservation ?
- Give me an insight on the reservation on reunion room ?
- I want an insight on the reservation
- Give me an insight on room
- I need an insight on the room
- Tell me an insight on the reservation
- I would like an insight on the reservation
- Could I have an insight on the reservation ?
- I'd like an insight on the reservation
- I really like an insight on the reservation

## intent: more_insight_yearly
- [yearly](typeof_insight)
- By [year](typeof_insight)

## intent: more_insight_all
- [All](typeof_insight) insight
- I want [all](typeof_insight) the insight available

## intent: more_insight_monthly
- [monthly](typeof_insight)
- By [month](typeof_insight)

## intent: more_insight_daily
- By [day](typeof_insight)
- [daily](typeof_insight)

## intent: insight_day
- [08/08/2019](slot_insight_day)
- [05/08/2019](slot_insight_day)
- [01/09/2009](slot_insight_day)


## intent: insight_month
- Give me the insight for [August 2018](slot_insight_month)
- For [09/2012](slot_insight_month)
- [08/2019](slot_insight_month)

## intent: insight_year
- Give me the insight for the [2018](slot_insight_year)
- For the year [2012](slot_insight_year)
- During year [2019](slot_insight_year)
- For the [2018](slot_insight_year)
- During year [2017](slot_insight_year)
- During year [2019](slot_insight_year)
- Give me the insight for the [2019](slot_insight_year)




## intent:thanks
- Thanks
- Thank you so much
- Thanks for that
- cheers
- cheers bro
- ok thanks!
- perfect thank you
- thanks a bunch for everything
- thanks for the help
- amazing, thanks
- cool, thanks

## intent:what_can_you_do
- What can you do ?
- What can you do for me ?
- How can you help me ?
- How do I use you ?
- How could you help me ?
- Can you tell me your services ? 
- What kind of services can you give me ? 
- Tell me what you can do as services ? 
- What can you do for me ?
- What are your services ? 
