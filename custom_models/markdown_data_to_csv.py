import pandas as pd
import markdown
from bs4 import BeautifulSoup
import codecs

nlu_dataframe = pd.DataFrame()
labels = list()
sentences = list()


#input_file = codecs.open('../data/nlu_data.md', mode="r", encoding="utf-8")
input_file = codecs.open('../data_original/nlu_data.md', mode="r", encoding="utf-8")
md_file = input_file.read()
htmlmarkdown = markdown.markdown(md_file)

soup = BeautifulSoup(htmlmarkdown, 'html.parser')


for intent, ul_tag in dict(zip(soup.find_all("h2"), soup.find_all("ul"))).items():
    one_ul = BeautifulSoup(str(ul_tag), 'html.parser')
    for li_tag in one_ul.find_all("li"):
        sentences.append(li_tag.get_text())
        labels.append(intent.get_text())


print len(labels)
print len(sentences)

nlu_dataframe["labels"] = labels
nlu_dataframe["sentences"] = sentences

print nlu_dataframe


nlu_dataframe.to_csv("./nlu_dataframe.csv", sep=";")
