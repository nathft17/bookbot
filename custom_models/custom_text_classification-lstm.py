
# Chargement des librairies nécessaires

import pandas as pd
import os
import numpy as np
from time import time

from sklearn.linear_model import LogisticRegression
from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from keras.utils import to_categorical
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline

from keras.layers import Activation, Input, Conv1D, Dense, Embedding, Flatten, Input, MaxPooling1D, Dropout, Conv2D, LSTM, Embedding, Masking, AveragePooling1D, TimeDistributed
from keras.models import Sequential, Model
from keras.callbacks import EarlyStopping

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

#########################################################################
# Chargement du dataset 
data = pd.read_csv("custom_models/nlu_dataframe.csv", sep=";", index_col=0)



# Lister les labels (intentions) contenus dans le dataset
labels=data["labels"]
print("labels :", labels)
labels_unique= data["labels"].unique().tolist()
print("labels_unique :", labels_unique)

# Lister les phrases contenues dans le dataset
sentences=data["sentences"]
print("sentences : ", sentences)

# Encoding des labels en utilisant la méthode "LabelEncoder" de sklearn
lab_enc = LabelEncoder()
#entrainons notre label encodeur sur les classes de notre dataset

#transformation des labels de notre dataset
one_hot_labels = lab_enc.fit_transform(labels)
print("one_hot_labels : ", one_hot_labels)

print("liste labels : ", list(lab_enc.classes_))


# Pour connaitre l'association d'un encodage à un label :
#one_hot_labels_inverse = lab_enc.inverse_transform(labels)
#print("one_hot_labels_inverse : ", one_hot_labels_inverse)


# # label_str_output retourne le nom du label ("inverse" du label encoder)
# one_hot_labels = lab_enc.inverse_transform(label_output)
# print ("label_str_output : ", label_str_output)

categorical_label = to_categorical(one_hot_labels)
print ("categorical_label: ", categorical_label)
number_of_classes = len(set(one_hot_labels))
print("number_of_classes : ", number_of_classes)



# Splitting des données pour obtenir un dataset de train (80%) et un dataset de test (20%)
X_train, X_test, y_train, y_test = train_test_split(sentences, categorical_label, test_size=0.2, random_state=42)

print("X_train.shape :", X_train.shape, "y_train.shape :", y_train.shape)
print("X_test.shape", X_test.shape,"y_test.shape :", y_test.shape)


#####################################################################################
# Vectorisation des phrases (transformation des phrases string -> vecteur)
vectorizer = TfidfVectorizer(lowercase=True, max_features=10000, ngram_range= (1,2), max_df=1.0, stop_words='english', norm='l1')
print("TfidfVectorizer : ", vectorizer)
# Entrainons le sur les données
vectorizer.fit(X_train)
# Montrons le vocabulaire qu'il utilisera
print("vectorizer.vocabulary_ : ", vectorizer.vocabulary_)

# Puis transformons notre dataset (1 phrase = 1 vecteur)
# pour le dataset de train (dense = .toarray())
X_train = vectorizer.transform(X_train)
print("X_train :", X_train)
# et le dataset de test (sparse)
X_test = vectorizer.transform(X_test)
print("X_test :", X_test)

#vectors = vectorizer.transform(sentences).toarray()
#vectors = vectorizer.fit_transform(sentences)
#print("vectors :", vectors)


# X_train = np.reshape(X_train, (X_train.shape[0], 1, X_train.shape[1]))
# X_test = np.reshape(X_test, (X_test.shape[0], 1, X_test.shape[1]))



#####################################################################################



# Architecture de notre réseau de neurone

model = Sequential()


# Essai 1 : ############################@
# model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
# model.add(Dense(number_of_classes, activation='softmax'))
# model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
# epochs = 5
# batch_size = 64
# history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size,validation_split=0.1,callbacks=[EarlyStopping(monitor='val_loss', patience=3, min_delta=0.0001)])


### Essai 2 ############################
model.add(LSTM(24, input_shape=X_train.shape, return_sequences=True, implementation=2))
model.add(Dense(1))
model.add(AveragePooling1D())
model.add(Dense(number_of_classes, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam')
model.fit(X_train, y_train, epochs=100, batch_size=6000, verbose=1, validation_data=(X_test, y_test))


# résumé de l'architecture 
model.summary()

# Entrainement du modèle
model_output = model.fit(X_train, y_train, epochs=100, batch_size=5, verbose=1)

# # Test du modèle
# test_model = model.evaluate(X_test, y_test, batch_size=5, verbose=1)

# for score, metrics in dict(zip(test_model, model.metrics_names)).items() :
#     print ("Test : ", str(metrics) + " " + str(score))

# Evaluation des modèles
loss, accuracy = model.evaluate(X_train, y_train, verbose=False)
print("Training Accuracy: {:.4f}".format(accuracy))
print("Training loss: {:.4f}".format(loss))

loss, accuracy = model.evaluate(X_test, y_test, verbose=False)
print("Testing Accuracy:  {:.4f}".format(accuracy))
print("Training Loss: {:.4f}".format(loss))


# Grid Search


# Sauvegarde du modèle


# Chargement du modèle 


# Prédiction de la phrase "I want to book the red"
# vectorisation de la phrase en utilisant le vectorizer formé avec TfidfVectorizer (cf. ligne 37)
# pred = vectorizer.transform(["I want to book the red room"])
pred = vectorizer.transform(["I would like to arrange a reservation room"])
print("pred", pred)
# prédiction avec le modèle de classification entrainé ligne 65
model_pred = model.predict(pred)
print ("model_pred :", model_pred)

# label_output retourne le label qui a la probabilité la plus élevée
label_output = np.argmax(model_pred)
#label_output = 13
print("label_output :", label_output)

# label_str_output retourne le nom du label ("inverse" du label encoder)
# label_str_output = lab_enc.inverse_transform(label_output)
# print ("label_str_output : ", label_str_output)