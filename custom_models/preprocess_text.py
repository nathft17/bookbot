
# Chargement des librairies nécessaires

import pandas as pd
import os
import numpy as np
from time import time

from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from keras.utils import to_categorical
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, TfidfTransformer
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline

from keras.layers import Activation, Input, Conv1D, Dense, Embedding, Flatten, Input, MaxPooling1D, Dropout, Conv2D
from keras.models import Sequential, Model
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

#########################################################################
# Chargement du dataset 
data = pd.read_csv("custom_models/nlu_dataframe.csv", sep=";", index_col=0)



# Lister les labels (intentions) contenus dans le dataset
labels=data["labels"].tolist()
print("labels :", labels)
labels_unique= data["labels"].unique().tolist()
print("labels_unique :", labels_unique)

# Lister les phrases contenues dans le dataset
sentences=data["sentences"].tolist()
print("sentences : ", sentences)

# # Encoding des labels en utilisant la méthode "LabelEncoder" de sklearn
# lab_enc = LabelEncoder()
# #entrainons notre label encodeur sur les classes de notre dataset

# #transformation des labels de notre dataset
# one_hot_labels = lab_enc.fit_transform(labels)
# print("one_hot_labels : ", one_hot_labels)

# print("liste labels : ", list(lab_enc.classes_))


# # Pour connaitre l'association d'un encodage à un label :
# #one_hot_labels_inverse = lab_enc.inverse_transform(labels)
# #print("one_hot_labels_inverse : ", one_hot_labels_inverse)


# # # label_str_output retourne le nom du label ("inverse" du label encoder)
# # one_hot_labels = lab_enc.inverse_transform(label_output)
# # print ("label_str_output : ", label_str_output)

# categorical_label = to_categorical(one_hot_labels)
# print ("categorical_label: ", categorical_label)
# number_of_classes = len(set(one_hot_labels))
# print("number_of_classes : ", number_of_classes)



# Splitting des données pour obtenir un dataset de train (80%) et un dataset de test (20%)
#X_train, X_test, y_train, y_test = train_test_split(sentences, categorical_label, test_size=0.2, random_state=42)
X_train, X_test, y_train, y_test = train_test_split(sentences, labels, test_size=0.2, random_state=42)


print ("X_train :", X_train)
print ("y_train :", y_train)

# #####################################################################################
# # Vectorisation des phrases (transformation des phrases string -> vecteur)
# vectorizer = TfidfVectorizer(min_df=1, lowercase=True)
# print("TfidfVectorizer : ", vectorizer)
# # Entrainons le sur les données
# vectorizer.fit(X_train)
# # Montrons le vocabulaire qu'il utilisera
# print("vectorizer.vocabulary_ : ", vectorizer.vocabulary_)

# # Puis transformons notre dataset (1 phrase = 1 vecteur)
# # pour le dataset de train (dense = .toarray())
# X_train = vectorizer.transform(X_train).toarray()
# print("X_train :", X_train)
# # et le dataset de test (sparse)
# X_test = vectorizer.transform(X_test)
# print("X_test :", X_test)

#vectors = vectorizer.transform(sentences).toarray()
#vectors = vectorizer.fit_transform(sentences)
#print("vectors :", vectors)



# GRIDSEARCH TF IDF #############################################################################
# Définir une pipeline combinant une vectorizer avec un classifier simple 
pipeline = Pipeline([
    ('vect', TfidfVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', SGDClassifier(tol=1e-3)),
])

# vous pouvez décommenter les lignes pour avoir une meilleure exploration mais cela coutera plus en calcul
parameters = {
    'vect__max_df': (0.5, 0.75, 1.0),
    'vect__max_features': (None, 5000, 10000, 50000),
    'vect__ngram_range': ((1, 1), (1, 2)),  # unigrams or bigrams
    'tfidf__use_idf': (True, False),
    'tfidf__norm': ('l1', 'l2'),
    'clf__max_iter': (20,),
    'clf__alpha': (0.00001, 0.000001),
    'clf__penalty': ('l2', 'elasticnet'),
    'clf__max_iter': (10, 50, 80),
}


# find the best parameters for both the feature extraction and the
# classifier
grid_search = GridSearchCV(pipeline, parameters, cv=5,
                            n_jobs=-1, verbose=1)

print("Performing grid search...")
print("pipeline:", [name for name, _ in pipeline.steps])
print("parameters:")
print(parameters)
t0 = time()
grid_search.fit(X_train, y_train)
print("done in %0.3fs" % (time() - t0))
print()

print("Best score: %0.3f" % grid_search.best_score_)
print("Best parameters set:")
best_parameters = grid_search.best_estimator_.get_params()
for param_name in sorted(parameters.keys()):
    print("\t%s: %r" % (param_name, best_parameters[param_name]))