import mysql.connector
from datetime import datetime, time, timedelta
import dateparser
import pendulum
import string
import dateutil
from dateutil import *
from robot.libraries.DateTime import convert_time
from timefhuman import timefhuman
# pip3 install timefhuman => https://github.com/alvinwan/timefhuman => 



# function to return a tuple from the pendulum object type
def from_pendulum_to_tupple(date):
    print("date: {}".format(date))
    if (isinstance(date, datetime)):
        year = date.year
        month = date.month
        day = date.day
        hour = date.hour
        minute = date.minute
    else:
        date = dateutil.parser.parse(date,fuzzy=True)
        year = date.year
        month = date.month
        day = date.day
        hour = date.hour
        minute = date.minute
    return (year, month, day, hour, minute)

# function to check if the room asked is free while looking in the database
# def is_the_room_available(name_room, day_only, day_startinghour, day_ending_hour, cnx):
def is_the_room_available(name_room, day_only, timestamp, timestamp_plus_duration, cnx):
 
    # variables
    starting_hour_list = []
    ending_hour_list = []
    room_list = []

    #cursor
    cur_select_all = cnx.cursor(buffered=True)
    #query_select_all = ("SELECT * FROM reservation")
    query_select_all = ("SELECT * FROM reservation WHERE name_room='{0}'".format(name_room))
    cur_select_all.execute(query_select_all)


    # convert the entry starting and ending meeting hour to a tupple
    asked_starting_hour = from_pendulum_to_tupple(timestamp)
    asked_ending_hour = from_pendulum_to_tupple(timestamp_plus_duration)

    # select all the name room, starting and ending meeting hour and append them to a list
    # I wanted to unpack your tuple to make it easier to read
    # but I do not know how big your tuple is so I added the *rest syntax
    # which puts the rest of the elements in a new list called rest.
    #for x, room, start_time, end_time, *rest in cur_select_all:
    for id, room, start_time, end_time, *rest in cur_select_all:
        room_list.append(room)
        print("start_time: ", start_time)
        print("end_time: ", end_time)
        starting_hour_list.append(from_pendulum_to_tupple(start_time))
        ending_hour_list.append(from_pendulum_to_tupple(end_time))

    # loop through a list
    # if the asked room is equal to the room in the list
    # and if the asked_starting_hour and asked_ending_hour
    # are between the lists values return False else return True
    for i in range(len(starting_hour_list)):
        if name_room == room_list[i]:
            print('is the same room')
            if starting_hour_list[i] <= asked_starting_hour <= ending_hour_list[i]:
                print('starting hour is between')
                return False
            if starting_hour_list[i] <= asked_ending_hour <= ending_hour_list[i]:
                print('ending hour is between')
                return False

    return True


##### MAIN FUNCTION : Function Room Booking ######
def make_a_booking(name_room, day, hour_start, duration, name):

    print("make a booking with :", name_room, day, hour_start, duration, name)
    # connect to the localhost database
    cnx = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')

    #day_only : get the parsed date
    day_only = str(dateparser.parse(day).date())
    print("day_only : ", day_only)

    # Date conversion
    start_string = str(day + " at " + hour_start)
    timestamp = timefhuman(start_string)
    duration_in_seconds = convert_time(duration)
    duration_in_minutes = duration_in_seconds / 60
    timestamp_plus_duration = timestamp + timedelta(minutes=duration_in_minutes)
    print(timestamp)
    print(timestamp_plus_duration)

    # check if the room is available
    #room_available = is_the_room_available(name_room, day_only, pendulum_combined_day_and_hour_start, pendulum_combined_day_and_hour_end, cnx)
    room_available = is_the_room_available(name_room, day_only, timestamp, timestamp_plus_duration, cnx)


    #if the room isn't available return False
    # else make the insert intro the database and return True
    if room_available == False:
        print('Oh wait ..., I just checked and unfortunately this room is not available :-(')
        return False
    else:
        print('Hey, I just checked and the room is available :-)')
        cur_insert_entry = cnx.cursor(buffered=True)
        query_insert_one = ("INSERT INTO reservation (name_room, hour_start, hour_end, name, idemployee, idroom) "
        "VALUES ('{0}', '{1}', '{2}', '{3}' ,"
        "(SELECT idemployee FROM employee WHERE name = '{4}'),"
        "(SELECT idroom FROM room WHERE name_room = '{5}'))" .format(name_room, timestamp, timestamp_plus_duration, name, name, name_room))
        print(" the query is " + str(query_insert_one))
        cur_insert_entry.execute(query_insert_one)
        cnx.commit()
        cur_insert_entry.close()
        cnx.close()
        return True

#make_a_booking("blue", "tomorrow", "10h00", "1h45min", "nisha")



# function to clean the database
def clean_the_data_base():
    cnx_delete = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')
    cur_delete = cnx_delete.cursor(buffered=True)
    delete_query = ("DELETE FROM reservation")
    cur_delete.execute(delete_query)
    cnx_delete.commit()
    cur_delete.close()
    cnx_delete.close()

# print all the entries of the database
def print_all():
    cnx_for_select_all = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')
    cur_select_all = cnx_for_select_all.cursor(buffered=True)
    query_select_all = ("SELECT * FROM reservation")
    cur_select_all.execute(query_select_all)
    #for i in cur_select_all:
        #print(i)
    cur_select_all.close()



#clean_the_data_base()

#print('print all the values')
#print_all()

