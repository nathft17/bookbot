INSERT INTO reservation (name_room, hour_start, hour_end, name, idemployee, idroom)
VALUES ('red', '2019-07-04T08:00:00+00:00', '2019-07-04T09:00:00+00:00', 'michel',
(SELECT idemployee FROM employee WHERE name = 'michel'),
(SELECT idroom FROM room WHERE name_room = 'red'));

SELECT * FROM workbot.reservation;