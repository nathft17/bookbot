CREATE DATABASE  IF NOT EXISTS `alex` ;
USE `alex`;
-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: alex
-- ------------------------------------------------------
-- Server version	8.0.11


SET NAMES utf8 ;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
 SET character_set_client = utf8mb4 ;
 set @@sql_mode='no_engine_substitution';
CREATE TABLE `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_room` varchar(45) DEFAULT NULL,
  `hour_start` varchar(45) DEFAULT NULL,
  `hour_end` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


