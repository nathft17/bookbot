import mysql.connector
from datetime import datetime, time, timedelta
import dateparser
import pendulum
import string
import dateutil
from dateutil import *
from robot.libraries.DateTime import convert_time
from timefhuman import timefhuman
#pip3 install timefhuman => https://github.com/alvinwan/timefhuman 



# function to return a tuple from the pendulum object type
def from_pendulum_to_tupple(date):
    print("date: {}".format(date))
    if (isinstance(date, datetime)):
        year = date.year
        month = date.month
        day = date.day
        hour = date.hour
        minute = date.minute
    else:
        date = dateutil.parser.parse(date,fuzzy=True)
        year = date.year
        month = date.month
        day = date.day
        hour = date.hour
        minute = date.minute
    return (year, month, day, hour, minute)


# main function
def make_a_vacation(day, hour_start, duration, name):
    print("make a vacation saving")

    print(day, hour_start, duration, name)
    # connect to the localhost database
    cnx = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')

    #day_only : get the parsed date
    day_only = str(dateparser.parse(day).date())

    # Date conversion
    start_string = str(day + " at " + hour_start)
    timestamp = timefhuman(start_string)
    duration_in_seconds = convert_time(duration)
    duration_in_minutes = duration_in_seconds / 60
    timestamp_plus_duration = timestamp + timedelta(minutes=duration_in_minutes)
    print(timestamp)
    print(timestamp_plus_duration)

    # # parse the hour in string inputed by the user and convert it the a pendulum object
    # hour_start_parsed = dateutil.parser.parse(hour_start, fuzzy_with_tokens=True)
    # pendulum_combined_day_and_hour_start = pendulum.parse(str(day_only) + " " + hour_start, strict=False)

    # # convert the duration in string inputed by the user and to seconds then in minutes
    # duration_in_seconds = convert_time(duration)
    # duration_in_minutes = duration_in_seconds / 60

    # # add the duration_in_minutes to the starting hour to get the hour start pendulum object
    # pendulum_combined_day_and_hour_end = pendulum_combined_day_and_hour_start.add(minutes = duration_in_minutes)
    # #print(pendulum_combined_day_and_hour_end)

    # save the vacation time

    # Check for remaining days
    cur_insert_entry_check_available_days = cnx.cursor(buffered=True)

    remaining_days_query = "SELECT vac FROM employee WHERE name = '{0}'".format(name)

    cur_insert_entry_check_available_days.execute(remaining_days_query)
    remaining_days = cur_insert_entry_check_available_days.fetchone()[0]
    print("remaining_days : ", remaining_days)
    remaining_days = float(remaining_days)
    print("type of remaining_days :", type(remaining_days))

    duration_in_hour = duration_in_minutes / 60
    duration_in_days = duration_in_hour / 24
    print("duration_in_days", duration_in_days)

    # if there is enough remaining days:
    if duration_in_days < remaining_days:
        # then the vacation booking can be done : 
        print("You have enough remaining day ! We can book your vacations")
        
        # query to insert vacation booking
        cur_insert_entry = cnx.cursor(buffered=True)
        query_insert_one = ("INSERT INTO vacations (day_start, day_end, idemployee_vac) "
        "VALUES ('{0}', '{1}',"
        "(SELECT idemployee FROM employee WHERE name = '{2}'))".format(timestamp, timestamp_plus_duration, name, name))
        print(" the query is " + str(query_insert_one))
        cur_insert_entry.execute(query_insert_one)
        cnx.commit()
        cur_insert_entry.close()
        print('Your vacation time is saved !')

        # query to update the number of remaining days :
        cur_update_remaining_days_entry = cnx.cursor(buffered=True)
        # calculate new value of remaining days:
        remaining_days_new = remaining_days - duration_in_days
        print("remaining_days_new: ", remaining_days_new)
        query_update_remaining_days = ("UPDATE employee SET vac = '{0}' WHERE(name = '{1}')".format(remaining_days_new, name))
        #UPDATE `workbot`.`employee` SET `vac` = '45' WHERE (`idemployee` = '11');

        cur_update_remaining_days_entry.execute(query_update_remaining_days)
        cnx.commit()
        cur_update_remaining_days_entry.close()
        print('Your remaining days of vacation time is saved !')



        # but if there is not enough remaining days:
    else :
        print("Oups, you ask for '{0}' days but you only have '{1}' remaining days".format(duration_in_days, remaining_days))

        cnx.close()

    return True


make_a_vacation(day = "12/01/2019", hour_start = "12:00", duration = "30 days", name = "nathalie")

# function to clean the database
def clean_the_data_base():
    cnx_delete = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')
    cur_delete = cnx_delete.cursor(buffered=True)
    delete_query = ("DELETE FROM vacations")
    cur_delete.execute(delete_query)
    cnx_delete.commit()
    cur_delete.close()
    cnx_delete.close()

# print all the entries of the database
def print_all():
    cnx_for_select_all = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')
    cur_select_all = cnx_for_select_all.cursor(buffered=True)
    query_select_all = ("SELECT * FROM vacations")
    cur_select_all.execute(query_select_all)
    #for i in cur_select_all:
        #print(i)
    cur_select_all.close()
