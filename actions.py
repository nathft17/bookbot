# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

# Importation des bibliothèques nécessaires
import logging
import requests
import json
import mysql

import dateutil.parser as dparser

from rasa_core_sdk import Action
from booking import make_a_booking
from insight import get_insight_on_room_occupency
from insight import return_message
from rasa_sdk import Action
#from booking import make_a_booking_2019
from mysql import connector
from pymongo import MongoClient



import datetime
import time

logger = logging.getLogger(__name__)


import nltk
nltk.download('vader_lexicon')
from nltk.sentiment.vader import SentimentIntensityAnalyzer

# importing nltk: a Natural Language Toolkit
import nltk

# importing 
# import ssl

# try:
#     _create_unverified_https_context = ssl._create_unverified_context
# except AttributeError:
#     pass
# else:
#     ssl._create_default_https_context = _create_unverified_https_context

# nltk.download()
# nltk.download('vader_lexicon')
# from nltk.sentiment.vader import SentimentIntensityAnalyzer

class ActionJoke(Action):
    def name(self):
        # define the name of the action which can then be included in training stories
        return "action_joke"

    def run(self, dispatcher, tracker, domain):
        # what your action should do
        request = json.loads(requests.get('https://api.chucknorris.io/jokes/random').text)  # make an api call
        joke = request['value']  # extract a joke from returned json response
        dispatcher.utter_message(joke)  # send the message back to the user
        return []



def get_user_sentiment(self, text):
    sid = SentimentIntensityAnalyzer()
    res = sid.polarity_scores(text)
    print(res)
    return res


class SentimentAnalyser(Action):
    def name(self):
        return "action_sentiment"

    def run(self,dispatcher, tracker, domain):
        print(tracker.current_slot_values()) # {u'duration': None, u'hour_start': None, u'name_room': None, u'day': u'today', u'name': None}

        for key, value in tracker.current_state().items():
            print(str(key) + " => " + str(value))
            print("_____________________________")


        def parse_the_tracker(current_state):
            return_state = {}
            if "latest_action_name" in current_state.keys():
                return_state["latest_action_name"] = current_state["latest_action_name"]

            if len(current_state["latest_message"]["entities"]) != 0:
                return_state["name"] = current_state["slots"]["name"]
                return_state["entity_confidence"] = current_state["latest_message"]["entities"][0]["confidence"]
                return_state["entity_value"] = current_state["latest_message"]["entities"][0]["value"]
                return_state["entity_type"] = current_state["latest_message"]["entities"][0]["entity"]
                return_state["entity_start"] = current_state["latest_message"]["entities"][0]["start"]
                return_state["entity_end"] = current_state["latest_message"]["entities"][0]["end"]

            return_state["name"] = current_state["slots"]["name"]
            return_state["intent_confidence"] =  current_state["latest_message"]["intent_ranking"][0]["confidence"]
            return_state["intent_name"] =  current_state["latest_message"]["intent_ranking"][0]["name"]
            return_state["user_message"] = current_state["latest_message"]["text"]
            return_state["time_epoch"] = time.time()
            return_state["time_str"] = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

            # Add sentiment analysis
            sid = SentimentIntensityAnalyzer()
            res = sid.polarity_scores(return_state["user_message"])

            # Insertion in MySQL
            # Connexion à la base de données
            cnx = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')

            cur_insert_entry = cnx.cursor(buffered=True)
            query_insert_one = ("INSERT INTO sentiments "
            "(name, latest_action_name, intent_confidence, intent_name, user_message, time_epoch, time_str, sentiment) "
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')" .format(return_state["name"], return_state["latest_action_name"],
            return_state["intent_confidence"], return_state["intent_name"], return_state["user_message"], return_state["time_epoch"], return_state["time_str"], res))
            
            print(" the query to add discussion is " + str(query_insert_one))

            print("before execution")
            cur_insert_entry.execute(query_insert_one)
            print("after execution")

            print("before commit")

            cnx.commit()

            print("after commit")

            cur_insert_entry.close()
            
            cnx.close()

            print ("connexion closed")

            return return_state

        return_state = parse_the_tracker(tracker.current_state())
        print("========> return_state after parse =", return_state)

        return []


class ActionRepeat(Action):
    def name(self):
        return "action_repeat"

    def run(self,dispatcher, tracker, domain):
        print(tracker.current_slot_values()) # {u'duration': None, u'hour_start': None, u'name_room': None, u'day': u'today', u'name': None}

        # for key, value in tracker.current_state().items():
        #     print(str(key) + " => " + str(value))
        #     print("_____________________________")

        # connexion to MongoDB 
        #client = MongoClient('localhost', 27017)
        #db = client.users_posts
        #collection = db.posts

        def parse_the_tracker(current_state):
            return_state = {}
            if "latest_action_name" in current_state.keys():
                return_state["latest_action_name"] = current_state["latest_action_name"]

            if len(current_state["latest_message"]["entities"]) != 0:
                return_state["name"] = current_state["slots"]["name"]
                return_state["entity_confidence"] = current_state["latest_message"]["entities"][0]["confidence"]
                return_state["entity_value"] = current_state["latest_message"]["entities"][0]["value"]
                return_state["entity_type"] = current_state["latest_message"]["entities"][0]["entity"]
                return_state["entity_start"] = current_state["latest_message"]["entities"][0]["start"]
                return_state["entity_end"] = current_state["latest_message"]["entities"][0]["end"]

            return_state["name"] = current_state["slots"]["name"]
            return_state["intent_confidence"] =  current_state["latest_message"]["intent_ranking"][0]["confidence"]
            return_state["intent_name"] =  current_state["latest_message"]["intent_ranking"][0]["name"]
            return_state["user_message"] = current_state["latest_message"]["text"]
            return_state["time_epoch"] = time.time()
            return_state["time_str"] = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

            # Add sentiment analysis
            sid = SentimentIntensityAnalyzer()
            res = sid.polarity_scores(return_state["user_message"])
            res_compound = res["compound"]

            # Insertion in MySQL
            # Connexion à la base de données
            cnx = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')

            cur_insert_entry = cnx.cursor(buffered=True)
            query_insert_one = ("INSERT INTO discussions "
            "(name, latest_action_name, intent_confidence, intent_name, user_message, time_epoch, time_str, sentiment) "
            "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')" .format(return_state["name"], return_state["latest_action_name"],
            return_state["intent_confidence"], return_state["intent_name"], return_state["user_message"], return_state["time_epoch"], return_state["time_str"], res_compound))
            
            print(" the query to add discussion is " + str(query_insert_one))

            print("before execution")
            cur_insert_entry.execute(query_insert_one)
            print("after execution")

            print("before commit")

            cnx.commit()

            print("after commit")

            cur_insert_entry.close()
            
            cnx.close()

            print ("connexion closed")

            return return_state

        return_state = parse_the_tracker(tracker.current_state())
        print("========> return_state after parse =", return_state)

        # Insertion in MongoDB 
        #collection.insert_one(return_state)

        return []

class ActionBookRoom(Action):
    def name(self):
        return 'action_room'

    def run(self, dispatcher, tracker, domain):
        # Fonction permettant d'enregistrer les entités name_room, day, hour_start, duration, name
        # et de faire appel à la fonction "make_a_booking" pour enregistrer la réservation dans la base de données 
        print("inside run")

        # Enregistrement des entités : name_room, day, hour_start, duration, name
        name_room = tracker.get_slot('name_room')
        day = tracker.get_slot('day')
        hour_start = tracker.get_slot('hour_start')
        duration = tracker.get_slot('duration')
        name = tracker.get_slot('name')

        # Enregistrement de la réservation dans la base de données en faisant appel à la fonction make_a_booking
        print("before booking_answer")        
        booking_answer = make_a_booking(name_room, day, hour_start, duration, name)
        print("booking_answer : " + str(booking_answer))
        # Vérification de la disponibilité de la salle à l'heure indiquée dans la base de données et réponse de disponibilité
        if booking_answer:
            booking_answer = 'The reservation has been made'
        else:
            booking_answer = 'The room is taken at this hour'
        
        # Réponse récapitulative à l'utilisateur du bot pour qu'il puisse vérifier que les éléments de réservation sont corrects
        response = """You want to book the {} room on {} at {} for {}. Is it correct {} ?""".format(name_room, day, hour_start, duration, name)
        
        name_room = str(name_room)
        day = str(day)
        hour_start = str(hour_start)
        duration = str(duration)
        name = str(name)
        print("before connexion")
        
        # SQL queries #
        # Connexion à la base de données
        try:
            cnx = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')
            print("before connexion")
            cursor = cnx.cursor()

            # Ajout de la nouvelle réservation
            add_booking = ("INSERT INTO reservation "
                          "(name_room, hour_start, hour_end, name, idemployee, idroom) "
                          "VALUES ('{0}', '{1}', '{2}', '{3}'," 
                          "(SELECT idemployee FROM employee WHERE name = '{4})" 
                          "(SELECT idroom FROM room WHERE name_room = '{5}'))" .format(name_room, hour_start, duration, name, name, name_room)
                          )
            print("before execute")
            #cursor.execute(add_booking)

        # Si erreur de connection, information sur la nature de l'erreur
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
        else:
            cnx.close()
        print("after connexion") 

        # Réponse envoyée par le bot
        dispatch = dispatcher.utter_message(response)
        dispatch = dispatcher.utter_message(str(booking_answer))
        
        return dispatch


class ActionVacation(Action):
    def name(self):
        return 'action_vacation'

    def run(self, dispatcher, tracker, domain):
        # Function saving entities day, hour_start, duration, name
        # and call the imported function "make_a_vacation" saving the booking in the database 
        print("inside run")

        # Saving entities : day, hour_start, duration, name
        day = tracker.get_slot('day')
        hour_start = tracker.get_slot('hour_start')
        duration = tracker.get_slot('duration')
        name = tracker.get_slot('name')

        # Saving booking elements and prepare the response to the user using the imported function "vacation_answer"
        print("before vacation_answer")        
        vacation_answer = vacation_answer(day, hour_start, duration, name)
        print("vacation_answer : " + str(vacation_answer))
        
        # Response to the user checking informations
        response = """You want to book vacation time on {} at {} for {}. Is it correct {} ?""".format(day, hour_start, duration, name)
        
        day = str(day)
        hour_start = str(hour_start)
        duration = str(duration)
        name = str(name)
        print("before connexion")
        
        # SQL queries #
        # Connexion to mysql database
        try:
            cnx = mysql.connector.connect(password='jeb41jeb', user="root", database="workbot", auth_plugin='mysql_native_password')
            print("before connexion")
            cursor = cnx.cursor()

            # Add a new booking
            
            add_booking = ("INSERT INTO vacations "
                          "(day_start, day_end, idemployee_vac) "
                          "VALUES ('{0}', '{1}'," 
                          "(SELECT idemployee FROM employee WHERE name = '{2}'))".format(hour_start, duration, name, name)
                          )
            print("before execute")
            #cursor.execute(add_booking)

        # Managing mysql connector errors
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
        else:
            cnx.close()
        print("after connexion") 

        # response sent by the bot to the user
        dispatch = dispatcher.utter_message(response)
        dispatch = dispatcher.utter_message(str(booking_answer))
        
        return dispatch


class get_insight(Action):
    def name(self):
        return "action_get_insight"

    def run(self, dispatcher, tracker, domain):
        by_year_occupency, by_month_occupency, by_day_occupency = get_insight_on_room_occupency()

        year_slot = tracker.get_slot("slot_insight_year")
        month_slot = tracker.get_slot("slot_insight_month")
        day_slot = tracker.get_slot("slot_insight_day")

        year_slot_str = str(year_slot)
        month_slot_str = str(month_slot)
        day_slot_str = str(day_slot)
        print(year_slot_str)
        print(month_slot_str)
        print(day_slot_str)

        if year_slot_str != "None":
            if year_slot_str in by_year_occupency:
                response = return_message(year_slot_str, by_year_occupency)
            else:
                response = "In {}, no reservation was made".format(year_slot_str)
            dispatch = dispatcher.utter_message(response)

        elif month_slot_str != "None":
            if month_slot_str in by_month_occupency:
                response = return_message(month_slot_str, by_month_occupency)
            else:
                response = "In {}, no reservation was made".format(month_slot_str)
            dispatch = dispatcher.utter_message(response)

        elif day_slot_str != "None":
            print(day_slot_str)
            print(by_day_occupency)
            if day_slot_str in by_day_occupency:
                response = return_message(day_slot_str, by_day_occupency)
                print(response)
            else:
                response = "In {}, no reservation was made".format(day_slot_str)
            dispatch = dispatcher.utter_message(response)

        else:
            for k, v in by_year_occupency.items():
                dispatch = dispatcher.utter_message(
                    "For the year: {}, the rooms where booked for {} corresponding to {}% of occupency over this period of time "
                    .format(str(k), str(v[0]), str(v[1])))
            for k, v in by_month_occupency.items():
                dispatch = dispatcher.utter_message(
                    "For the month: {}, the rooms where booked for {} corresponding to {}% of occupency over this period of time "
                        .format(str(k), str(v[0]), str(v[1])))
            for k, v in by_day_occupency.items():
                dispatch = dispatcher.utter_message(
                    "For the day: {}, the rooms where booked for {} corresponding to {}% of occupency over this period of time "
                        .format(str(k), str(v[0]), str(v[1])))

        return dispatch


class ActionAskRemainingVacationDays(Action):
    def name(self):
        return 'action_ask_remaining_vacation_days'

    def run(self, dispatcher, tracker, domain):
        #Function saving name and call the imported
        #function "ask_remaining_vacation_days" with request to db for vacation days
        print("inside run (ask_remaining_vacation_days)")

        # Saving entity : name
        name = tracker.get_slot('name')

        # Saving vacation days elements and prepare the response to the user
        # using the imported function "ask_remaining_vacation_days"
        print("ask_remaining_vacation_days")
        ask_remaining_vacation_days_answer = ask_remaining_vacation_days(name)
        print("ask_remaining_vacation_days_answer :", + str(ask_remaining_vacation_days_answer))

        # Response to the user checking informations
        dispatch = dispatcher.utter_message(str)

        return dispatch



