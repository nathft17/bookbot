
import nltk

from nltk.sentiment.vader import SentimentIntensityAnalyzer


def get_user_sentiment(text):
    sid = SentimentIntensityAnalyzer()
    res = sid.polarity_scores(text)
    res_compound = res["compound"]
    print("res :", res)
    print("res_compound : ", res_compound)
    return res_compound

#text = "go fuck yourself"
#text = "hello"
text = "happy"
get_user_sentiment(text)